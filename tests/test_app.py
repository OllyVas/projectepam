import os
import unittest
import tempfile
import random
import app
import string
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from models.employee import Employee
from models.department import Department




class TestCheck(unittest.TestCase):

    def test_index(self):
        tester = app.app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
    
    def test_database(self):
        tester = os.path.exists("app.db")
        self.assertEqual(tester, True)


def get_random_string(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

class TestEndpoints(unittest.TestCase):

    def setUp(self):
        app.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        self.db = SQLAlchemy(app.app)
        migrate = Migrate(app.app, self.db)
        self.db.create_all()
        app.app.config['TESTING'] = True
        self.app = app.app.test_client()

    def test_departments_get(self):
        rv = self.app.get('/')
        assert b'Departments' in rv.data

    def test_department_create(self):
        dp_name = get_random_string(8)
        dp_adress = get_random_string(8)

        self.app.post('/create/department', data=dict(
            name = dp_name,
            email = "email@gml.com",
            adress = dp_adress,
            phone = "000-000-0000"
        ), follow_redirects=True)
        rv = self.app.get('/')
        assert dp_name.encode() in rv.data
        assert dp_adress.encode() in rv.data

    def test_department_update(self):
        self.db.session.query(Department).delete()
        self.db.session.commit()
        dp_name = get_random_string(8)
        dp_adress = get_random_string(8)

        self.app.post('/create/department', data=dict(
            name = dp_name,
            email = "email@gml.com",
            adress = dp_adress,
            phone = "000-000-0000"
        ), follow_redirects=True)

        new_name = get_random_string(8)
        new_adress = get_random_string(8)
        self.app.post('/update/department/1', data=dict(
            name = new_name,
            email = "email@gml.com",
            adress = new_adress,
            phone = "000-000-0000"), follow_redirects=True)

        rv = self.app.get('/')
        assert new_name.encode() in rv.data
        assert new_adress.encode() in rv.data

    def test_employee_create(self):
        self.db.session.query(Employee).delete()
        self.db.session.commit()
        name = get_random_string(8)
        position = get_random_string(8)
        self.app.post('/create/employee', data=dict(
            name = name,
            position = position,
            born_date = "2000-01-01",
            salary = 1000,
            department = 1
            ), follow_redirects=True)

        rv = self.app.get('/employees')
        assert name.encode() in rv.data
        assert position.encode() in rv.data

    def test_employee_update(self):
        self.db.session.query(Employee).delete()
        self.db.session.commit()
        name = get_random_string(8)
        position = get_random_string(8)

        self.app.post('/create/employee', data=dict(
            name = name,
            position = position,
            born_date = "2000-01-01",
            salary = 1000,
            department = 2
            ), follow_redirects=True)

        name_new = get_random_string(8)
        position_new = get_random_string(8)
        self.app.post('/update/employee/1', data=dict(
            name = name_new,
            position = position_new,
            born_date = "2000-01-01",
            salary = 1000,
            department = 1
            ), follow_redirects=True)
        rv = self.app.get('/employees')
        assert name_new.encode() in rv.data
        assert position_new.encode() in rv.data


    def test_employees_by_department(self):
        self.db.session.query(Department).delete()
        self.db.session.query(Employee).delete()
        self.db.session.commit()
        self.app.post('/create/department', data=dict(
            name = "name",
            email = "email@gml.com",
            adress = "adress",
            phone = "000-000-0000"
        ), follow_redirects=True)

        name1 = get_random_string(8)
        name2 = get_random_string(8)
        self.app.post('/create/employee', data=dict(
            name = name1,
            position = "position",
            born_date = "2000-01-01",
            salary = 1000,
            department = 1
            ), follow_redirects=True)
        
        self.app.post('/create/employee', data=dict(
            name = name2,
            position = "position",
            born_date = "2000-01-01",
            salary = 1000,
            department = ""
            ), follow_redirects=True)
        
        rv = self.app.get('/employees/by_department/1')
        assert name1.encode() in rv.data
        assert name2.encode() not in rv.data

    def test_employees_by_date(self):
        name1 = get_random_string(8)
        name2 = get_random_string(8)
        self.app.post('/create/employee', data=dict(
            name = name1,
            position = "position",
            born_date = "2010-01-01",
            salary = 1000,
            department = 1
            ), follow_redirects=True)
        
        self.app.post('/create/employee', data=dict(
            name = name2,
            position = "position",
            born_date = "2000-01-01",
            salary = 1000,
            department = 1
            ), follow_redirects=True)
        
        rv = self.app.get('/employees')
        assert name1.encode() in rv.data
        assert name2.encode() in rv.data

        rv = self.app.post('/employees/by_date', data=dict(
            from_date = "2005-01-01",
            to_date = "2015-01-01",
            ), follow_redirects=True)
        assert name1.encode() in rv.data
        assert name2.encode() not in rv.data

    def test_employee_delete(self):
        self.db.session.query(Employee).delete()
        self.db.session.commit()
        name = get_random_string(8)
        
        self.app.post('/create/employee', data=dict(
            name = name,
            position = "position",
            born_date = "2000-01-01",
            salary = 1000,
            department = 1
            ), follow_redirects=True)

        rv = self.app.get('/employees')
        assert name.encode() in rv.data
        self.app.get('/delete/employee/1')
        rv = self.app.get('/employees')
        assert name.encode() not in rv.data

    def test_department_delete(self):
        self.db.session.query(Department).delete()
        self.db.session.commit()
        name = get_random_string(8)

        self.app.post('/create/department', data=dict(
            name = name,
            email = "email@gml.com",
            adress = "adress",
            phone = "000-000-0000"
            ), follow_redirects=True)

        rv = self.app.get('/')
        assert name.encode() in rv.data
        self.app.get('/delete/department/1')
        rv = self.app.get('/')
        assert name.encode() not in rv.data


    def test_department_avg_salary(self):
        self.db.session.query(Department).delete()
        self.db.session.query(Employee).delete()
        self.db.session.commit()
        name = get_random_string(8)
        rv = self.app.get('/')
        assert name.encode() not in rv.data

        self.app.post('/create/department', data=dict(
            name = name,
            email = "email@gml.com",
            adress = "adress",
            phone = "000-000-0000"
            ), follow_redirects=True)

        rv = self.app.get('/')
        assert name.encode() in rv.data
        assert "755.0".encode() not in rv.data

        self.app.post('/create/employee', data=dict(
            name = "name",
            position = "position",
            born_date = "2000-01-01",
            salary = 1000,
            department = 1
            ), follow_redirects=True)
        
        self.app.post('/create/employee', data=dict(
            name = "name",
            position = "position",
            born_date = "2000-01-01",
            salary = 510,
            department = 1
            ), follow_redirects=True)
        
        rv = self.app.get('/')
        assert name.encode() in rv.data
        assert "755.0".encode() in rv.data


if __name__ == '__main__':
    unittest.main()