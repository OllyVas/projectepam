import sys
import models
from sqlalchemy.orm import relationship, backref
sys.path.append("..")
from app import db

class Department(db.Model):
    __tablename__ = 'departments'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    adress = db.Column(db.String(120), unique=False, nullable=False)
    email = db.Column(db.String(120), unique=False, nullable=False)
    phone = db.Column(db.Unicode(20), unique=False, nullable=False)
    employees = relationship("Employee", backref=backref("department", uselist = True))
    
    def __repr__(self):
        return '<Department %r>' % self.name

    def __init__(self, name, email = '', adress = '', phone = ''):
        self.name = name
        self.email = email
        self.adress = adress
        self.phone = phone