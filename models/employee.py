import sys
import models
sys.path.append("..")
from app import db

class Employee(db.Model):
    __tablename__ = 'employees'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    position = db.Column(db.String(120), unique=False, nullable=False)
    born_date = db.Column(db.Date, nullable=True)
    salary = db.Column(db.Integer, nullable=False, unique=False)
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'), nullable=True)

    def __repr__(self):
        return '<Employee %r>' % self.name

    def __init__(self, name, position, born_date, salary = 0, department_id = None):
        self.name = name
        self.born_date = born_date
        self.position = position
        self.salary = salary
        self.department_id = department_id

    
