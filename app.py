from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime
from sqlalchemy.sql import func
import logging
import os

logging.basicConfig(filename="app.log", level=logging.INFO)
log = logging.getLogger("App logs: ")
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'

db = SQLAlchemy(app)
migrate = Migrate(app, db)

print("Starting...")

from models.employee import Employee
from models.department import Department

"""
Creating of DB
"""
@app.before_first_request
def create_tables():
    db.create_all()
    log.info('Tables created.')


"""
Basic page is a list of departments
"""
@app.route('/', methods=['GET'])
def get_departments():
    departments = (
        db.session.query(Department.id, Department.name,
        Department.email, Department.adress, Department.phone,
        func.count(Employee.id).label("emp_num"),
        func.avg(Employee.salary).label("salary")
        )
        .outerjoin(Employee)
        .group_by(Department.id)
        .all()
    )
    log.info('Get list of departments.')
    return render_template("departments_view.html", departments=departments)

"""
List of employees, joined with departments(for getting name)
"""
@app.route('/employees', methods=['GET'])
def get_employees():
    employees = (
        db.session.query(Employee.id, Employee.name, 
        Employee.born_date, Employee.position, 
        Employee.salary, Department.name.label("department"))
        .outerjoin(Department)
        .order_by(Employee.name)
        .all()
    )
    log.info('Get list of employees.')
    return render_template("employees_view.html", employees=employees)

"""
List of employees, filtered by two dates, joined with departments(for getting label)
"""
@app.route('/employees/by_date', methods=['POST'])
def get_employees_by_date():
    from_date = datetime.strptime(request.form['from_date'],'%Y-%M-%d')
    to_date = datetime.strptime(request.form['to_date'],'%Y-%M-%d')
    if to_date < from_date:
        to_date, from_date = from_date, to_date
    
    employees = (
        db.session.query(Employee.id, Employee.name, 
        Employee.born_date, Employee.position, 
        Employee.salary, Department.name.label("department"))
        .outerjoin(Department)
        .filter(Employee.born_date <= to_date)
        .filter(Employee.born_date >= from_date)
        .order_by(Employee.name)
        .all()
    )
    log.info('Get list of employees by date.')
    return render_template("employees_view.html", employees=employees)

"""
List of employees, for special department
"""
@app.route('/employees/by_department/<int:id>', methods=['GET'])
def get_employees_by_department(id):
    employees = (
        db.session.query(Employee.id, Employee.name, 
        Employee.born_date, Employee.position, 
        Employee.salary, Department.name.label("department"))
        .outerjoin(Department)
        .filter(Department.id == id)
        .order_by(Employee.name)
        .all()
    )
    log.info('Get list of employees by department {id}.')
    return render_template("employees_view.html", employees=employees)

"""
Getting page and creating new department
"""
@app.route('/create/department', methods=['GET', 'POST'])
def create_department():
    if request.method == 'GET':
        return render_template("update_department.html")
    elif request.method == 'POST':
        dp_name = request.form['name']
        dp_email = request.form['email']
        dp_adress = request.form['adress']
        dp_phone = request.form['phone']
        new_dp = Department(dp_name, email=dp_email, adress=dp_adress, phone=dp_phone)
        try:
            db.session.add(new_dp)
            db.session.commit()
            log.info('Creating new department.')
            return redirect('/')
        except:
            log.exception('There was an issue creating new department!')
            return 'There was an issue adding your department'
    return 'Method not found!'

"""
Getting page and updating department by id
"""
@app.route('/update/department/<int:id>', methods=['GET', 'POST'])
def update_department(id):
    if request.method == 'GET':
        department = Department.query.filter(Department.id == id).first()
        return render_template("update_department.html", department=department)
    else:
        dp_name = request.form['name']
        dp_email = request.form['email']
        dp_adress = request.form['adress']
        dp_phone = request.form['phone']
        try:
            rowcount = db.session.query(Department).filter_by(id=id).update({"name" : dp_name, "email" : dp_email, "adress" : dp_adress, "phone" : dp_phone})
            db.session.commit()
            log.info('Updating department {id}.')
            return redirect('/')
        except:
            log.exception('There was an issue updating your department {id}!')
            return 'There was an issue updating your department'
    return 'Method not found!'

"""
Deleting department with clearing departmentId fields in employees 
"""
@app.route('/delete/department/<int:id>')
def delete_department(id):
    try:
        Employee.query.filter(Employee.department_id == id).update({"department_id" : None})
        Department.query.filter(Department.id == id).delete()
        db.session.commit()
        log.info('Deleting department {id}.')
    except:
        log.exception('There was an issue deleting your department {id}!')
        return 'There was an issue deleting your department'
    return redirect('/')

"""
Getting page and creating new employee
"""
@app.route('/create/employee', methods=['GET', 'POST'])
def create_employee():
    if request.method == 'GET':
        departments = Department.query.order_by(Department.name).all()
        return render_template("update_employee.html", employee=None, departments=departments)
    elif request.method == 'POST':
        em_name = request.form['name']
        em_position = request.form['position']
        em_born_date = datetime.strptime(request.form['born_date'],'%Y-%M-%d')
        em_salary = request.form['salary']
        em_department = request.form['department'] if request.form['department'] else None
        
        new_em = Employee(em_name, em_position, em_born_date, em_salary, em_department)
        try:
            db.session.add(new_em)
            db.session.commit()
            log.info('Creating new employee.')
            return redirect('/employees')
        except:
            log.exception('There was an issue creating new employee!')
            return 'There was an issue adding your employee'
    return 'Method not found!'

"""
Getting page and updating employee by id
"""
@app.route('/update/employee/<int:id>', methods=['GET', 'POST'])
def update_employee(id):
    if request.method == 'GET':
        departments = Department.query.order_by(Department.name).all()
        employee = Employee.query.filter(Employee.id == id).first()
        return render_template("update_employee.html", employee=employee, departments=departments)
    else:
        em_name = request.form['name']
        em_position = request.form['position']
        em_born_date = datetime.strptime(request.form['born_date'],'%Y-%M-%d')
        em_salary = request.form['salary']
        em_department = request.form['department'] if request.form['department'] else None
        try:
            rowcount = db.session.query(Employee).filter_by(id=id)\
                .update({"name" : em_name,"position" : em_position,\
                    "born_date" : em_born_date,"salary" : em_salary,\
                    "department_id" : em_department})
            db.session.commit()
            log.info('Updating employee {id}.')
            return redirect('/employees')
        except:
            log.exception('There was an issue updating your employee {id}!')
            return 'There was an issue updating your employee'
    return 'Method not found!'

"""
Deleting employee
"""
@app.route('/delete/employee/<int:id>')
def delete_employee(id):
    try:
        rows_deleted = Employee.query.filter(Employee.id == id).delete()
        db.session.commit()
        log.info('Deleting employee {id}.')
    except:
        log.exception('There was an issue deleting {id} employee!')
        return 'There was an issue deleting your employee'
    return redirect('/employees')

if __name__ == '__main__':
    app.run()
